package com.rsmtwo.getallthecoins;

import java.util.ArrayList;
import java.util.List;

import com.rsmtwo.getallthecoins.scene.BestScoreScene;
import com.rsmtwo.getallthecoins.scene.GameScene;
import com.rsmtwo.getallthecoins.scene.SplashScene;

import ca.qc.cvm.cvmandengine.scene.CVMAbstractScene;
import ca.qc.cvm.cvmandengine.ui.CVMGameActivity;

public class GetALLTheCoins extends CVMGameActivity {
	
	public GetALLTheCoins() {
		super(TextureMgr.getInstance());
		super.setSoundManager(SoundMgr.getInstance());
		
		List<CVMAbstractScene> sceneList = new ArrayList<CVMAbstractScene>();
		sceneList.add(new SplashScene());
		sceneList.add(new GameScene());
		sceneList.add(new BestScoreScene());
		
		super.setSceneList(sceneList);
	}

}
