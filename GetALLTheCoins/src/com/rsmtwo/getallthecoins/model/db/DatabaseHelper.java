package com.rsmtwo.getallthecoins.model.db;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseHelper extends SQLiteOpenHelper {
	private static final String DATBASE_NAME = "db_scores";
	private static final int DATABASE_VERSION = 1;
	
	public DatabaseHelper(Context context) {
		super(context, DATBASE_NAME, null, DATABASE_VERSION);
	}
	
	@Override
	public void onCreate(SQLiteDatabase db) {
		try {
			//Cr�ation des tables
			db.execSQL("CREATE TABLE playerScores (" + 
					   "  id        INTEGER PRIMARY KEY AUTOINCREMENT," +
					   "  score 	INTEGER," +
					   "  time		REAL" +
						")");
			Log.i("DB", "DB created");
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("");
		onCreate(db);
	}

}