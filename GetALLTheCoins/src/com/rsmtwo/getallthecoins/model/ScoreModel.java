package com.rsmtwo.getallthecoins.model;

import java.text.DecimalFormat;

import com.rsmtwo.getallthecoins.model.db.DatabaseHelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class ScoreModel {
	private DatabaseHelper databaseHelper;
	private SQLiteDatabase database;
	
	public ScoreModel(Context ctx) {
		databaseHelper = new DatabaseHelper(ctx);
	}
	
	public void openDatabase() {
		database = databaseHelper.getWritableDatabase();
	}
	
	public void closeDatabase() {
		database.close();
	}
	
	public long addScore(int playerScore, float time) {
		ContentValues values = new ContentValues();
		values.put("score", playerScore);
		values.put("time", time);
		
		return database.insert("playerScores", null, values);
	}
	
	public String getBestScore() {
		int score = 0;
		try {
			Cursor cursor = database.query(true, "playerScores", new String[]{"id", "score", "time"}, null, null, null, null, "score", null);
			if(cursor != null) {
				cursor.moveToLast();
				score = cursor.getInt(1);
			}
		} catch(CursorIndexOutOfBoundsException e) {
			Log.i("Score Model", "Requesting Score on empty database");
		}
		
		return String.valueOf(score);
	}
	
	public String getBestTime() {
		float time = 0.0f;
		DecimalFormat df = new DecimalFormat("#.##");
		Cursor cursor = database.query(true, "playerScores", new String[]{"id", "score", "time"}, null, null, null, null, "time", null);
		try {
			if(cursor != null) {
				cursor.moveToLast();
				time = cursor.getFloat(2);
			}
		} catch(CursorIndexOutOfBoundsException e) {
			Log.i("Score Model", "Requesting Time on empty database");
		}
		
		return df.format(time);
	}
}
