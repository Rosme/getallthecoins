package com.rsmtwo.getallthecoins;

import ca.qc.cvm.cvmandengine.CVMTextureManager;
import ca.qc.cvm.cvmandengine.entity.CVMTexture;

public class TextureMgr extends CVMTextureManager {
	private static TextureMgr instance = null;
	
	/* Player */
	public static final int PLAYER = 0;
	
	/* Items */
	public static final int COIN = 1;
	public static final int LASER = 2;
	public static final int TOMBSTONE = 3;
	
	/* Menu items */
	public static final int START_BUTTON = 10;
	public static final int BESTSCORE_BUTTON = 11;
	public static final int EXIT_BUTTON = 12;
	public static final int LOGO = 13;
	
	/* Backgrounds */
	public static final int FIELD_BACKGROUND = 50;
	
	/* Particles */
	public static final int PARTICLE_FIRE = 100;
	public static final int PARTICLE_POINT = 101;
	
	private TextureMgr() {
		super.addTexture(new CVMTexture(COIN, "images/coins.png", 80, 28, 4, 1));
		super.addTexture(new CVMTexture(PLAYER, "images/player.png", 252, 48, 9, 1));
		super.addTexture(new CVMTexture(START_BUTTON, "images/start_button.png", 128, 52));
		super.addTexture(new CVMTexture(BESTSCORE_BUTTON, "images/bestscore_button.png", 215, 31));
		super.addTexture(new CVMTexture(EXIT_BUTTON, "images/exit_button.png", 113, 35));
		super.addTexture(new CVMTexture(FIELD_BACKGROUND, "background/back.png", 800, 480));
		super.addTexture(new CVMTexture(LASER, "images/laser.png", 50, 13));
		super.addTexture(new CVMTexture(LOGO, "images/logo.png", 276, 275));
		super.addTexture(new CVMTexture(PARTICLE_FIRE, "particles/particle_fire.png", 32, 32));
		super.addTexture(new CVMTexture(TOMBSTONE, "images/tombstone.png", 39, 45));
		super.addTexture(new CVMTexture(PARTICLE_POINT, "particles/particle_point.png", 32, 32));
	}
	
	public static TextureMgr getInstance() {
		if(instance == null) {
			instance = new TextureMgr();
		}
		
		return instance;
	}
}
