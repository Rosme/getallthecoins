package com.rsmtwo.getallthecoins;

import ca.qc.cvm.cvmandengine.CVMSoundManager;
import ca.qc.cvm.cvmandengine.entity.CVMSound;

public class SoundMgr extends CVMSoundManager {
	private static SoundMgr instance = null;
	
	/* Constants */
	public static final int COIN = 1;
	public static final int LASER = 2;
	public static final int JUMP = 3;
	public static final int EXPLOSION = 4;
	
	private SoundMgr() {
		super.addSound(new CVMSound(COIN, "sounds/coin.wav"));
		super.addSound(new CVMSound(LASER, "sounds/laser.wav"));
		super.addSound(new CVMSound(JUMP, "sounds/jump.wav"));
		super.addSound(new CVMSound(EXPLOSION, "sounds/explosion.wav"));
	}
	
	public static SoundMgr getInstance() {
		if(instance == null) {
			instance = new SoundMgr();
		}
		
		return instance;
	}
}
