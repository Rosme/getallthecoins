package com.rsmtwo.getallthecoins.scene;

import org.andengine.input.touch.TouchEvent;
import org.andengine.util.color.Color;

import android.util.Log;

import com.rsmtwo.getallthecoins.model.ScoreModel;
import com.rsmtwo.getallthecoins.sprite.LogoSprite;

import ca.qc.cvm.cvmandengine.entity.CVMText;
import ca.qc.cvm.cvmandengine.scene.CVMAbstractScene;

public class BestScoreScene extends CVMAbstractScene {
	
	/* Members */
	private CVMText score = null;
	private CVMText time = null;
	
	public BestScoreScene() {
		super("background/back.png", 2);
		
		super.addSprite(new LogoSprite());
	}

	@Override
	public void managedUpdate(float elapsedSeconds) {
	}

	@Override
	public void sceneTouched(TouchEvent touchEvent) {
		
	}

	@Override
	public void starting() {
		ScoreModel model = new ScoreModel(this.gameActivity);
		model.openDatabase();
		score = new CVMText(480, 150, 25, "Best Score: " + model.getBestScore(), Color.YELLOW);
		time = new CVMText(480, 300, 25, "Best Time: " + model.getBestTime() + " seconds", Color.YELLOW);
		model.closeDatabase();
		super.addText(score);
		super.addText(time);
	}
}
