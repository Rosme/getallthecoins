package com.rsmtwo.getallthecoins.scene;

import java.text.DecimalFormat;
import java.util.Random;

import org.andengine.input.touch.TouchEvent;
import org.andengine.util.color.Color;

import android.util.Log;

import com.rsmtwo.getallthecoins.ParticlesFactory;
import com.rsmtwo.getallthecoins.model.ScoreModel;
import com.rsmtwo.getallthecoins.sprite.BackgroundSprite;
import com.rsmtwo.getallthecoins.sprite.CoinSprite;
import com.rsmtwo.getallthecoins.sprite.LaserSprite;
import com.rsmtwo.getallthecoins.sprite.PlayerSprite;
import com.rsmtwo.getallthecoins.sprite.TombStoneSprite;

import ca.qc.cvm.cvmandengine.entity.CVMText;
import ca.qc.cvm.cvmandengine.scene.CVMAbstractScene;
import ca.qc.cvm.cvmandengine.ui.CVMGameActivity;

public class GameScene extends CVMAbstractScene {
	/* Constant */
	public static final int PLAY_STATUS = 0;
	public static final int GAMEOVER_STATUS = 1;
	
	/* Members */
	private PlayerSprite player = null;
	private Random randGen = null;
	private float coinTimer;
	private float laserTimer;
	private CVMText scoreCounter = null;
	private int level = 0;
	private int status;
	private BackgroundSprite back1 = null; //For scrolling background
	private BackgroundSprite back2 = null; //For scrolling background
	
	
	public GameScene() {
		super("background/back.png", 1);
		randGen = new Random();
		coinTimer = 0.0f;
		laserTimer = 0.0f;
		
		back1 = new BackgroundSprite(0, 0);
		back2 = new BackgroundSprite(800, 0);
		super.addSprite(back1);
		super.addSprite(back2);
		
		scoreCounter = new CVMText(550, 10, 30, "Score: 0      ", Color.YELLOW);
		super.addText(scoreCounter);
	}

	@Override
	public void managedUpdate(float elapsedSeconds) {
		sortChildren();
		if(status == PLAY_STATUS) {
			coinTimer += elapsedSeconds;
			laserTimer += elapsedSeconds;
			
			level = player.getScore()/50;
			
			if(coinTimer > 4) {
				generateCoin();
				coinTimer = 0.0f;
			}
			
			float laserSpawnTime = 5.0f - level*0.2f;
			if(laserSpawnTime < 0.5f) {
				laserSpawnTime = 0.5f;
			}
			
			if(laserTimer > laserSpawnTime) {
				generateLaser();
				laserTimer = 0.0f;
			}
		} else {
			back1.stopScrolling();
			back2.stopScrolling();
		}
	}

	@Override
	public void sceneTouched(TouchEvent touchEvent) {
		if(touchEvent.getAction() == TouchEvent.ACTION_DOWN) {
			if(!player.isJumping()) {
				player.jump();
			} else {
				player.accelerateFall();
			}
		}
	}

	@Override
	public void starting() {
		status = PLAY_STATUS;
		coinTimer = 0.0f;
		laserTimer = 0.0f;
		player = new PlayerSprite(-50, 300);
		super.addSprite(player);
		this.gameActivity.setMusic("music/running.wav");
		back1.startScrolling();
		back2.startScrolling();
	}
	
	private void generateCoin() {
		Log.i("GameScene", "Coin generated");
		int y = randGen.nextInt(211);
		int nb = randGen.nextInt(5); //We'll have between 4 and 8 coins
		for(int i = 0; i < (nb+4); ++i) {
			super.addSprite(new CoinSprite(810+(23*i), y+120));
		}
	}
	
	public PlayerSprite getPlayer() {
		return player;
	}
	
	public CVMText getScoreCounter() {
		return scoreCounter;
	}
	
	public void generateLaser() {
		float spawnY = player.getY()+(player.getSprite().getHeight()/2);
		ParticlesFactory.addLaserHalo(CVMGameActivity.CAMERA_WIDTH-25, spawnY-5, this);
		super.addSprite(new LaserSprite(CVMGameActivity.CAMERA_WIDTH+300, spawnY));
	}
	
	public void setSceneStatus(int status) {
		if(status == GAMEOVER_STATUS) {
			ScoreModel model = new ScoreModel(this.gameActivity);
			model.openDatabase();
			model.addScore(player.getScore(), player.getTotalTime());
			model.closeDatabase();
			super.removeText(scoreCounter);
			DecimalFormat df = new DecimalFormat("#.##");
			super.addText(new CVMText(150, 110, 40, "Score: " + String.valueOf(player.getScore()), Color.YELLOW));
			super.addText(new CVMText(210, 230, 40, "Running time: " + df.format(player.getTotalTime()) + " seconds", Color.YELLOW));
		}
		this.status = status;
	}
	
	public int getSceneStatus() {
		return status;
	}
}
