package com.rsmtwo.getallthecoins.scene;

import org.andengine.input.touch.TouchEvent;

import com.rsmtwo.getallthecoins.sprite.LogoSprite;
import com.rsmtwo.getallthecoins.sprite.button.ExitButtonSprite;
import com.rsmtwo.getallthecoins.sprite.button.BestScoreButtonSprite;
import com.rsmtwo.getallthecoins.sprite.button.StartButtonSprite;

import ca.qc.cvm.cvmandengine.scene.CVMAbstractScene;

public class SplashScene extends CVMAbstractScene {
	public SplashScene() {
		super("background/back.png", 0);
		
		super.addSprite(new LogoSprite());
		super.addSprite(new StartButtonSprite());
		super.addSprite(new BestScoreButtonSprite());
		super.addSprite(new ExitButtonSprite());
	}
	
	@Override
	public void managedUpdate(float elapsedSeconds) {
	}

	@Override
	public void sceneTouched(TouchEvent touchEvent) {
	}

	@Override
	public void starting() {
		this.gameActivity.setMusic("music/running.wav");
	}

}
