package com.rsmtwo.getallthecoins.sprite;

import org.andengine.entity.sprite.AnimatedSprite;

import android.util.Log;

import com.rsmtwo.getallthecoins.SoundMgr;
import com.rsmtwo.getallthecoins.TextureMgr;
import com.rsmtwo.getallthecoins.scene.GameScene;

import ca.qc.cvm.cvmandengine.entity.CVMSprite;
import ca.qc.cvm.cvmandengine.entity.CollisionListener;
import ca.qc.cvm.cvmandengine.entity.ManagedUpdateListener;
import ca.qc.cvm.cvmandengine.scene.CVMAbstractScene;
import ca.qc.cvm.cvmandengine.ui.CVMGameActivity;

public class CoinSprite extends CVMSprite implements ManagedUpdateListener, CollisionListener {
	/* Constant */
	private final float VELOCITY = -3.5f;
	
	private boolean started;
	private float x;
	
	public CoinSprite(float x, float y) {
		super(x, y, 25, 35, TextureMgr.COIN);
		this.x = x;
		started = false;
	}

	@Override
	public void managedUpdate(float elapsedSeconds, CVMGameActivity activity,
			CVMAbstractScene scene) {
		if(!started) {
			started = true;
			AnimatedSprite as = ((AnimatedSprite)super.getSprite());
			as.animate(150, true);
		}
		
		x += VELOCITY;
		super.getSprite().setX(x);
		
		if(x < 0) {
			Log.i("Coin", "Not Grabed, Remove from scene");
			scene.removeSprite(this);
		}
		
		if(((GameScene)scene).getSceneStatus() == GameScene.GAMEOVER_STATUS) {
			scene.removeSprite(this);
		}
	}

	@Override
	public void collidedWith(CVMGameActivity activity, CVMAbstractScene scene,
			CVMSprite sprite) {
		if(sprite instanceof PlayerSprite) {
			PlayerSprite player = ((GameScene)scene).getPlayer();
			player.addScore(10);
			((GameScene)scene).getScoreCounter().getText().setText("Score: " + String.valueOf(player.getScore()));
			SoundMgr.getInstance().playSound(SoundMgr.COIN);
			scene.removeSprite(this);
		}
	}
	
}
