package com.rsmtwo.getallthecoins.sprite;

import com.rsmtwo.getallthecoins.TextureMgr;

import ca.qc.cvm.cvmandengine.entity.CVMSprite;

public class LogoSprite extends CVMSprite {
	/* Method */
	public LogoSprite() {
		super(50, 50, 414, 412, TextureMgr.LOGO);
	}

}
