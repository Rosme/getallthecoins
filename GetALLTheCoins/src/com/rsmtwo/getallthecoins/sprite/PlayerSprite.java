package com.rsmtwo.getallthecoins.sprite;

import org.andengine.entity.sprite.AnimatedSprite;

import android.util.Log;

import com.rsmtwo.getallthecoins.SoundMgr;
import com.rsmtwo.getallthecoins.TextureMgr;

import ca.qc.cvm.cvmandengine.entity.CVMSprite;
import ca.qc.cvm.cvmandengine.entity.ManagedUpdateListener;
import ca.qc.cvm.cvmandengine.scene.CVMAbstractScene;
import ca.qc.cvm.cvmandengine.ui.CVMGameActivity;

public class PlayerSprite extends CVMSprite implements ManagedUpdateListener {
	/* Constants */
	private final float VELOCITY = -6.3f;
	private final float GRAVITY = 0.1f;
	private final float MAXX = 60;
	private final float SPEEDX = 3.f;
	
	/* Members */
	private boolean started; //For animation
	private float x, y, minY; //Position in x and y of the player. minY is the minimum height the player is gonna be, from ground
	private float currentVelocity; //Current velocity based on gravity
	private int gravityModifier; //Usefull to make the gravity bigger
	private boolean jumping; //In case the player is jumping
	private AnimatedSprite as = null; //For animation
	private int score;
	private float totalTime;
	
	public PlayerSprite(float x, float y) {
		super(x, y, 42, 72, TextureMgr.PLAYER);
		this.x = x;
		this.y = y;
		minY = y;
		jumping = false;
		started = false;
		score = 0;
		totalTime = 0;
		gravityModifier = 1;
		currentVelocity = VELOCITY;
	}

	@Override
	public void managedUpdate(float elapsedSeconds, CVMGameActivity activity,
			CVMAbstractScene scene) {
		totalTime += elapsedSeconds;
		if(x < MAXX) {
			x += SPEEDX;
			super.getSprite().setX(x);
		}
		
		if(!started) {
			started = true;
			/*AnimatedSprite*/ as = ((AnimatedSprite)super.getSprite());
			as.animate(75, true);
			super.getSprite().setZIndex(100);
		}
		
		//Jumping
		if(jumping) {
			as.stopAnimation();
			currentVelocity += GRAVITY*gravityModifier;
			y += currentVelocity;
			
			//Applying gravity again to make it go down faster
			if(currentVelocity >= 0) {
				//Not applying modifier here. The idea is only to speed up just a little bit the fall
				currentVelocity += GRAVITY;
			}
			
			//We've reached the ground again
			if(y >= minY) {
				Log.i("Player", "Ground Reached");
				currentVelocity = VELOCITY;
				gravityModifier = 1;
				y = minY;
				jumping = false;
				as.animate(50, true);
			}
			
			super.getSprite().setY(y);
		}
	}

	public void jump() {
		if(!jumping) {
			Log.i("Player", "Jump Initiated");
			jumping = true;
			SoundMgr.getInstance().playSound(SoundMgr.JUMP);
		}
	}
	
	public boolean isJumping() {
		return jumping;
	}
	
	public void accelerateFall() {
		Log.i("Player", "Gravity changed");
		++gravityModifier;
	}
	
	public void addScore(int amount) {
		Log.i("Player", "Getting coins, getting rich. Scores UP!");
		score += amount;
	}
	
	public int getScore() {
		return score;
	}
	
	public float getY() {
		return y;
	}
	
	public float getMinY() {
		return minY;
	}
	
	public float getX() {
		return x;
	}
	
	public float getTotalTime() {
		return totalTime;
	}
}
