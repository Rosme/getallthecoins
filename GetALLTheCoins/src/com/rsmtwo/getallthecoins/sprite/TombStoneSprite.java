package com.rsmtwo.getallthecoins.sprite;

import com.rsmtwo.getallthecoins.ParticlesFactory;
import com.rsmtwo.getallthecoins.TextureMgr;

import ca.qc.cvm.cvmandengine.entity.CVMSprite;
import ca.qc.cvm.cvmandengine.entity.ManagedUpdateListener;
import ca.qc.cvm.cvmandengine.scene.CVMAbstractScene;
import ca.qc.cvm.cvmandengine.ui.CVMGameActivity;

public class TombStoneSprite extends CVMSprite implements ManagedUpdateListener{
	/* Constants */
	private final float GRAVITY = 7.0f;
	
	/* Members */
	private float y;
	private float playerMaxY; //Usefull to know when to stop making the tombstone fall
	private boolean onGround; //Tells the tombstone if we've reached the ground
	
	/* Method */
	
	public TombStoneSprite(float playerX) {
		//x is player maximum, y is above the screen
		super(playerX, -50, 39, 52, TextureMgr.TOMBSTONE);
		this.y = -50;
		this.playerMaxY = 300;
		this.onGround = false;
	}


	@Override
	public void managedUpdate(float elapsedSeconds, CVMGameActivity activity,
			CVMAbstractScene scene) {
		if(!onGround) {
			y += GRAVITY;
			
			if(y >= playerMaxY) {
				y = playerMaxY;
				onGround = true;
			}
			super.getSprite().setY(y);
		}
	}

}
