package com.rsmtwo.getallthecoins.sprite;

import android.util.Log;

import com.rsmtwo.getallthecoins.ParticlesFactory;
import com.rsmtwo.getallthecoins.SoundMgr;
import com.rsmtwo.getallthecoins.TextureMgr;
import com.rsmtwo.getallthecoins.scene.GameScene;

import ca.qc.cvm.cvmandengine.entity.CVMSprite;
import ca.qc.cvm.cvmandengine.entity.CollisionListener;
import ca.qc.cvm.cvmandengine.entity.ManagedUpdateListener;
import ca.qc.cvm.cvmandengine.scene.CVMAbstractScene;
import ca.qc.cvm.cvmandengine.ui.CVMGameActivity;

public class LaserSprite extends CVMSprite implements ManagedUpdateListener, CollisionListener {
	/* Constant */
	private final float VELOCITY = -10.5f;
	
	/* Members */
	private float x;
	private boolean soundPlayed;
	
	/* Methods */
	
	public LaserSprite(float x, float y) {
		super(x, y, 50, 13, TextureMgr.LASER);
		this.x = x;
		this.soundPlayed = false;
	}

	@Override
	public void managedUpdate(float elapsedSeconds, CVMGameActivity activity,
			CVMAbstractScene scene) {
		if(x < CVMGameActivity.CAMERA_WIDTH) {
			if(!soundPlayed) {
				soundPlayed = true;
				SoundMgr.getInstance().playSound(SoundMgr.LASER);
			}
		}
		x += VELOCITY;
		super.getSprite().setX(x);
		
		if(x < 0-super.getSprite().getWidth()) {
			Log.i("Laser", "No touch player, removed");
			scene.removeSprite(this);
		}
	}

	@Override
	public void collidedWith(CVMGameActivity activity, CVMAbstractScene scene,
			CVMSprite sprite) {
		if(sprite instanceof PlayerSprite) {
			SoundMgr.getInstance().playSound(SoundMgr.EXPLOSION);
			PlayerSprite player = (PlayerSprite)sprite;
			ParticlesFactory.addExplosion(x-30, player.getY(), scene);
			scene.removeSprite(sprite);
			scene.removeSprite(this);
			((GameScene)scene).setSceneStatus(GameScene.GAMEOVER_STATUS);
			scene.addSprite(new TombStoneSprite(((PlayerSprite)sprite).getX()));
		}
	}
	
}
