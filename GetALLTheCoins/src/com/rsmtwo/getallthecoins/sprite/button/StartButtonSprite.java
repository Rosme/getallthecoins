package com.rsmtwo.getallthecoins.sprite.button;

import org.andengine.input.touch.TouchEvent;

import com.rsmtwo.getallthecoins.TextureMgr;

import ca.qc.cvm.cvmandengine.entity.CVMSprite;
import ca.qc.cvm.cvmandengine.entity.TouchAreaListener;
import ca.qc.cvm.cvmandengine.scene.CVMAbstractScene;
import ca.qc.cvm.cvmandengine.ui.CVMGameActivity;

public class StartButtonSprite extends CVMSprite implements TouchAreaListener {
	
	public StartButtonSprite() {
		super(540, 100, 128, 32, TextureMgr.START_BUTTON);
	}

	@Override
	public void onAreaTouched(TouchEvent touchEvent, float localX, float localY,
			CVMGameActivity activity, CVMAbstractScene scene) {
		activity.changeScene(1, true);
	}
}
