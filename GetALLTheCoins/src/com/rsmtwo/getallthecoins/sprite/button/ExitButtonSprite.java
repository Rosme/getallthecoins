package com.rsmtwo.getallthecoins.sprite.button;

import org.andengine.input.touch.TouchEvent;

import com.rsmtwo.getallthecoins.TextureMgr;

import ca.qc.cvm.cvmandengine.entity.CVMSprite;
import ca.qc.cvm.cvmandengine.entity.TouchAreaListener;
import ca.qc.cvm.cvmandengine.scene.CVMAbstractScene;
import ca.qc.cvm.cvmandengine.ui.CVMGameActivity;

public class ExitButtonSprite extends CVMSprite implements TouchAreaListener {
	
	public ExitButtonSprite() {
		super(543, 325, 113, 35, TextureMgr.EXIT_BUTTON);
	}

	@Override
	public void onAreaTouched(TouchEvent touchEvent, float localX, float localY,
			CVMGameActivity activity, CVMAbstractScene scene) {
		activity.finish();
	}
}
