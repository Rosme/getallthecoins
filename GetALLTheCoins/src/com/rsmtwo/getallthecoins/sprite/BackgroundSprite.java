package com.rsmtwo.getallthecoins.sprite;

import com.rsmtwo.getallthecoins.TextureMgr;

import ca.qc.cvm.cvmandengine.entity.CVMSprite;
import ca.qc.cvm.cvmandengine.entity.ManagedUpdateListener;
import ca.qc.cvm.cvmandengine.scene.CVMAbstractScene;
import ca.qc.cvm.cvmandengine.ui.CVMGameActivity;

public class BackgroundSprite extends CVMSprite implements ManagedUpdateListener {	
	/* Members */
	private float x;
	private float speed;
	
	/* Method */
	
	public BackgroundSprite(float x, float y) {
		super(x, y, 800, 480, TextureMgr.FIELD_BACKGROUND);
		this.x = x;
		speed = -1.5f;
	}

	@Override
	public void managedUpdate(float elapsedSeconds, CVMGameActivity activity,
			CVMAbstractScene scene) {
		x += speed;
		if(x <= -799) {
			x = 800;
		}
		super.getSprite().setX(x);
	}

	public void stopScrolling() {
		this.speed = 0.0f;
	}
	
	public void startScrolling() {
		this.speed = -1.5f;
	}
}
